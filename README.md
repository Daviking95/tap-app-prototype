# tap-app-prototype
A loan app built using Dart and Flutter SDK. Includes Biometrics, Authentication, and others

## App Distribution Link
- https://appdistribution.firebase.dev/i/d7ebbb03a50cc7c1

## App Screenshots
| Screenshots  | Screenshots  | Screenshots |
| :------------ |:---------------:| -----:|
| ![card-screen](/uploads/a8593b8294ba63c52460f9e7546c3b16/card-screen.png)      | ![dash1-screen](/uploads/99ceefcc7e9084e0cffea83ebed2d294/dash1-screen.png) | ![dash2-screen](/uploads/ca8ff4cf43da9dd5d8fe6df4d99fcd40/dash2-screen.png) |
| ![login-screen](/uploads/45948176ff474b82ab48a1af1f289c87/login-screen.png)      | ![otp-screen](/uploads/ef126253c7834b55d0b54becdd110e59/otp-screen.png)        |   ![payment-screen](/uploads/d864424cac7e6d86d4f6105c19634150/payment-screen.png) |
| ![profile-screen](/uploads/ac90204c4a7cd54750ca219f45b52067/profile-screen.png) | ![register-screen](/uploads/cbd0b09307223bc165f40a9de58c0596/register-screen.png)        |    ![start-screen](/uploads/fc3cf932f201dfc70caae67e58e7a53e/start-screen.png) |
| ![transaction-screen](/uploads/e889f2478e6692766fbb2a3f71178692/transaction-screen.png) |          |     |
